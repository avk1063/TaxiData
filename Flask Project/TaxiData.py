import pandas as pd
import operator
import matplotlib.pyplot as plt
import numpy as np

def readInput(filename):
    file = pd.read_csv(filename, low_memory=False)
    puLocationID = file['PULocationID']
    doLocationID = file['DOLocationID']
    pickupTime = file['lpep_pickup_datetime']
    tripFare = file['total_amount']
    columnNames = [puLocationID, doLocationID, pickupTime, tripFare]
    return columnNames


def readLookup(filename):
    file = pd.read_csv(filename)
    locationID = file['LocationID']
    borough = file['Borough']
    zone = file['Zone']
    serviceZone = file['service_zone']
    columnNames = [locationID, borough, zone, serviceZone]
    return columnNames


def computeFirst(inputColumns, lookupColumns):
    puLocationID, doLocationID = inputColumns[0], inputColumns[1]
    locationID, borough, zone = lookupColumns[0], lookupColumns[1], lookupColumns[2]
    countDictPU, countDictDO = {}, {}
    for index in range(len(puLocationID)):
        if puLocationID[index] not in countDictPU:
            countDictPU[puLocationID[index]] = 1
        else:
            countDictPU[puLocationID[index]] += 1
        if doLocationID[index] not in countDictDO:
            countDictDO[doLocationID[index]] = 1
        else:
            countDictDO[doLocationID[index]] += 1
    sortedPU = sorted(countDictPU.items(), key=operator.itemgetter(1))
    sortedDO = sorted(countDictDO.items(), key=operator.itemgetter(1))
    first, second = [], []
    third, fourth = [], []
    for index in range(1, 6):
        for idx in range(len(locationID)):
            if sortedPU[-index][0] == locationID[idx]:
                first.append(zone[idx])
                third.append(sortedPU[-index][1])
            if sortedDO[-index][0] == locationID[idx]:
                second.append(zone[idx])
                fourth.append(sortedDO[-index][1])

    xpos = np.arange(len(first))
    plt.figure(1, figsize=(7, 7.5))
    plt.subplot(211)
    plt.title('Most busiest pickup and drop-off locations')
    plt.bar(xpos, third, alpha=0.5, align='center', color='orange')
    plt.xticks(xpos, first, rotation=10)

    plt.subplot(212)
    plt.bar(xpos, fourth, alpha=0.5, align='center', color='blue')
    plt.xticks(xpos, second, rotation=10)
    plt.xlabel('Zones with highest frequencies')
    plt.ylabel('Count of taxi rides for a particular zone')

    plt.show()


def computeSecond(inputColumns):
    locationID, pickupTime, tripFare = inputColumns[0], inputColumns[2], inputColumns[3]
    countDict = {}
    countTrip = {}
    locationIDList = [5, 6, 23, 44, 84, 99, 109, 110, 115, 118, 156, 172, 176, 187, 204, 206, 214, 221, 245, 251]
    for index in range(len(pickupTime)):
        if locationID[index] in locationIDList:
            currentTime = str(pickupTime[index]).split(' ')
            hourOfDay = currentTime[1].split(':')
            if hourOfDay[0] not in countDict:
                countDict[hourOfDay[0]] = tripFare[index]
                countTrip[hourOfDay[0]] = 1
            else:
                countTrip[hourOfDay[0]] += 1
                countDict[hourOfDay[0]] += tripFare[index]
    first = (countDict['00'] + countDict['01'] + countDict['02'] + countDict['03'],
             countTrip['00'] + countTrip['01'] + countTrip['02'] + countTrip['03'])
    second = (countDict['04'] + countDict['05'] + countDict['06'] + countDict['07'],
              countTrip['04'] + countTrip['05'] + countTrip['06'] + countTrip['07'])
    third = (countDict['08'] + countDict['09'] + countDict['10'] + countDict['11'],
             countTrip['08'] + countTrip['09'] + countTrip['10'] + countTrip['11'])
    fourth = (countDict['12'] + countDict['13'] + countDict['14'] + countDict['15'],
              countTrip['12'] + countTrip['13'] + countTrip['14'] + countTrip['15'])
    fifth = (countDict['16'] + countDict['17'] + countDict['18'] + countDict['19'],
             countTrip['16'] + countTrip['17'] + countTrip['18'] + countTrip['19'])
    sixth = (countDict['20'] + countDict['21'] + countDict['22'] + countDict['23'],
             countTrip['20'] + countTrip['21'] + countTrip['22'] + countTrip['23'])
    yList = [first[0]/first[1], second[0]/second[1], third[0]/third[1], fourth[0]/fourth[1],
             fifth[0]/fifth[1], sixth[0]/sixth[1]]

    xlist = np.arange(len(yList))
    plt.figure(figsize=(7, 7.5))
    plt.plot(xlist, yList, '.b-', markersize=7, color='black')
    plt.title('Average trip fares for a time period in Staten Island')
    plt.xticks(xlist, ['Early morning', 'Morning', 'Early Afternoon', 'Afternoon', 'Evening', 'Late evening'], rotation=10)
    plt.xlabel('Time ranges of the day')
    plt.ylabel('Average trip fares')
    plt.show()


def main():
    inputColumns = readInput('green_tripdata_2017-12.csv')
    lookupColumns = readLookup('taxi+_zone_lookup.csv')
    computeFirst(inputColumns, lookupColumns)
    computeSecond(inputColumns)


if __name__ == '__main__':
    main()
